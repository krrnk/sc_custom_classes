// Pseudo-UGen
// Simple quadratic random &&|| sine LFO
RandSineLFO : PureUGen {
	*ar { arg minRange = -1, maxRange = 1, randomSignal = 1, randRate = 1, sineRate = 1;
		var randomLFO = LFNoise2.ar(randRate).range(minRange, maxRange) * randomSignal;
		var sineLFO = SinOsc.ar(sineRate).range(minRange, maxRange) * (1 - randomSignal);

		^(randomLFO + sineLFO);

	}

	*kr {arg minRange = -1, maxRange = 1, randomSignal = 1, randRate = 1, sineRate = 1;
		var randomLFO = LFNoise2.ar(randRate).range(minRange, maxRange) * randomSignal;
		var sineLFO = SinOsc.ar(sineRate).range(minRange, maxRange) * (1 - randomSignal);

		^(randomLFO + sineLFO);

	}
}

