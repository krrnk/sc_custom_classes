# README #

### Description ###

A collection of custom classes and pseudo-Ugens for SuperCollider

### Installation ###
Clone this repo to your SuperCollider Extensions directory

`
Platform.userExtensionDir.openOS;
`

### Contact ###
[email](mailto: info@krrnk.com)